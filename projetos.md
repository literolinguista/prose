
> “Não sou contra que você entre na universidade, mas não deixe isso atrapalhar seus estudos.”
>
> Mark Twain.

- [Informática, conceitos básicos](https://jacksondejesus.gitlab.io/projetos/informatica-conceitos-basicos/conceitos-basicos.html "Informática, conceitos básicos")
- [Haikais, publicação coletiva](https://jacksondejesus.gitlab.io/projetos/escrita/haiku-paula-antunes-sergio-borges-org-22-04-2021.pdf "Haikais, publicação coletiva")
- [Atividade de aula, Prevupe, 2021](https://jacksondejesus.gitlab.io/projetos/prevupe/index.html "Atividade de aula, Prevupe, 2021")
- [Redes livres: palestra relâmpago...](https://odysee.com/@jacksondejesus:1/palestraRelampagoJacksonJesusPyBR2021:2 "Redes livres: Palestra relâmpago na Python Brasil 2021")
- [UFPE no Meu Quintal, participação](https://www.youtube.com/watch?v=sjH2LXGlNQM "UFPE no Meu Quintal - Uma Semana em Dormentes")
- FLISol Recife 2019:
    - [Slide da apresentação](https://jacksondejesus.gitlab.io/projetos/flisolRecife/flisolRecife2019Apresentacao.html "Slide da apresentação")
    - [Texto da palestra](https://meiotexto.wordpress.com/2019/05/01/sobre-alternativas-livres-para-redes-sociais-proprietarias "Texto da palestra")
- [As cores dos discursos em sala de aula](https://prezi.com/fcpm0vwsmm09/as-cores-dos-discursos-em-sala-de-aula/?present=1 "As cores dos discursos em sala de aula") As cores dos discursos em sala de aula (vou refazer no Sozi)...

[Voltar ao início](https://literolinguista.prose.sh)
