
> “Ninguém consegue realizar nada sem a colaboração de muitos.”
>
>Paola Rhoden.

## Sites que [(...)](# "de alguma forma") me ajudaram a pensar e criar este site:

- [bashblog](https://github.com/cfenollosa/bashblog "bashblog")
- [debxp comunidade](https://debxp.org/ "debxp comunidade") 
- [Eduardo Cuducos](https://cuducos.me/ "Eduardo Cuducos") 
- [Elias Praciano](https://elias.praciano.com/ "Elias Praciano") 
- [freeCodeCamp](https://www.freecodecamp.org/ "freeCodeCamp") 
- [jdhao's digital space](https://jdhao.github.io/ "jdhao's digital space") 
- [Libre Designers](https://libredesigners.org/pt-br "Libre Designers") 
- [Olivia Maia](https://oliviamaia.net/ "Olivia Maia") 
- [Quinquilharias](https://ladina.neocities.org/ "Quinquilharias") 
- [rwx](https://rwx.gg "rwx") 
- [Solène Rapenne](https://dataswamp.org/~solene/index.html "Solène Rapenne") 
- [Terminal Root](https://terminalroot.com.br "Terminal Root") 
- [Treinaweb](https://treinaweb.com.br "TreinaWeb")
- [Trickster](https://trickster.dev "Trickster Dev") 
- [Willian Justen](https://willianjusten.com.br/ "Willian Justen") 
- [w3schools](https://www.w3schools.com/ "w3schools")

[Voltar ao início](https://literolinguista.prose.sh)
