[O áudio deste texto está disponível no Soundcloud](https://soundcloud.com/jackson-de-jesus-732931674/sobre-o-tempo)

# Sobre o tempo...

> Somos instantes...

... acessando a Internet, me deparei com esta matéria: [As 4 irmãs que tiraram uma foto por ano durante 36 anos](https://hypescience.com/as-irmas-brown-36-anos-em-fotografias/), então fiquei reflexivo [(...)](# "mais do que o normal"). Tanto que passei horas pensando em escrever muitas coisas, mas passando o tempo e pensando bem mesmo, decidi só deixar este arquivo no formato GIF feito com as imagens que baixei da postagem (a primeira foi de 1975 e a última de 2010).

![Imagem de quatro irmãs mudando com o passar do tempo...](https://gitlab.com/literolinguista/prose/-/raw/main/imagens/irmas400x319.gif "Imagem de quatro irmãs mudando com o passar do tempo...")

[Voltar para a página Textos](https://literolinguista.prose.sh/textos)
