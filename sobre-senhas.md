# Sobre senhas...

> – Ô de casa? – Quem é? – Sou eu? – Eu quem? – Fulano, não reconhece minha voz não é? – Ah, é você Fulano, entra aí…”

Pode não parecer, mas este diálogo é uma versão analógica de um processo digital muito comum, porém pouco cuidado pela maioria dos utilizadores de computador: a autenticação. Em informática, autenticação é, em resumo, um nome bonito para prova de identidade, ou seja, aquele velho e conhecido processo de digitar o login e a senha [(...)](# "que a gente sempre esquece").

Como no diálogo acima, onde alguém precisa provar sua identidade para ter acesso a determinado lugar, precisamos provar quem realmente somos para poder entrar em determinados sites e ter acesso aos seus recursos, como o Facebook, por exemplo; que inclusive tem seus dados de acesso usados em outro sites, o chamado login social.

O problema é que nem os utilizadores, no geral, nem o Facebook, especificamente, demonstram preocupações com a segurança dos processos de autenticação. A gente até diz que não temos nada a esconder e isso é tolerável, mas a empresa de dados ser denunciada por salvar as senhas de suas vítimas [(...)](# "quero dizer, utilizadores") em (acreditem!) texto puro! Uma irresponsabilidade...

Claro que já existem outras [(...)](# "diversas") maneiras de se autenticar, mas por enquanto, o esquema feijão com arroz [(...)](# "login e senha") ainda é [(...)](# "e provavelmente, continuará sendo por algum tempo") o mais usado. Então, como criar boas senhas? Este texto é sobre isso.

> – Besteira! Não preciso desse negócio de criar senhas, escrevo tudo no papel e depois…

E depois esquece onde guardou e perde tudo, eu sei, lembro bem como é…

Mas, o que é [(...)](# "ou deveria ser") uma boa senha? Apesar de ser uma noção intuitiva, essa pergunta é importante porque um dos grandes motivos do nosso esquecimento de senhas tem a ver justamente com parte de sua definição. Basicamente, uma senha é uma palavra ou código secreto e uma boa senha deve [(...)](# "ou deveria") ser também inesquecível.

Atualmente, os serviços que impõe algum processo de autenticação exigem senhas fortes, ou seja, para se considerada uma boa senha, elas devem ter pelo menos caracteres especiais e alfanuméricos em maiúsculas e minúsculas; daí a nossa dificuldade em memorizá-las.

Ora, se esta combinação é o ponto fraco de nossa memorização, a sugestão que proponho para a criação de senhas fortes é o uso de esquemas mnemônicos [(...)](# "adesivos para fixar informações na memória") com prefixos [(...)](# "palavras que vem antes"), infixos [(...)](# "palavras que vem no meio") e sufixos [(...)](# "palavras que vem no final"). Vou exemplificar com senhas antigas.

Quando eu era cozinheiro, além de trabalhar em outra língua [(...)](# "a francesa, no caso"), tinha acesso a uma gama de nomes de receitas e alimentos que geralmente são desconhecidos por quem não é da área. Naquela época, eu usava um prefixo com o nome de alguma erva em língua francesa, um infixo com algum caractere especial, seguido do nome da página em questão iniciada em letra maiúscula e por fim, um sufixo com o ano corrente da criação da senha. Não acompanhou o raciocínio? Se liga nos exemplos.

Senhas já usada nos sites Orkut, MySpace, Blogspot, Yahoo:

	romarin*Orkut2006
	romarin*MySpace2006
	romarin*Blogspot2006
	romarin*Yahoo2006i

	thym/Orkut2007
	thym/MySpace2007
	thym/Blogspot2007
	thym/Yahoo2007 

Em resumo: [erva, iniciada com letra minúscula] + [símbolo especial] + [Nome do site, iniciado com letra maiúscula] + [ano].

Capitou? Uma senha tem que ser difícil para quem quer tentar descobri-la [(...)](# "incluindo os programas de quebra de senhas"), mas fácil para seu criador. E uma das maneiras de criar senhas inesquecíveis tem a ver com torná-las significativas para quem as cria; lembrando que isso é um processo subjetivo. O esquema acima era significativo para mim, porque eu usava uma palavra associada ao meu trabalho, um símbolo especial qualquer, o nome do site que eu acessava e por fim o ano em que me encontrava quando criava a senha; esse esquema mnemônico eram os pregos de minha memória.

Mais uma vez, a fórmula pode ser:

Prefixo (nome significativo iniciado em minúscula) + símbolo especial (com alguma relação eufônica ou visualmente associado com o prefixo) + nome do site (primeira letra maiúscula) + ano de criação da senha (no formato desejado).

Eu digo “a fórmula pode ser” porque o ideal é que cada um crie seus próprios esquemas significativos, o modelo apresentado serve apenas como base e pode [(...)](# "e deve") ser totalmente subvertido de acordo com interesses pessoais, basta modificar a ordem, por exemplo; o limite é a imaginação [(...)](# "e a percepção de que realmente, não é difícil")!

Por fim, esta é uma sugestão para quem não quer usar os gerenciadores de senha [(...)](# "pessoalmente não gosto").

[Voltar para a página Textos](https://literolinguista.prose.sh/textos)

