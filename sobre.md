
> “Não sou contra nem a favor, muito pelo contrário...”
>
> Ditado popular.

Micro bio [(...)](# "não micróbio, pequena biografia").

Jackson é sotero-recifense [(...)](# "nascido em Salvador com um pé no Recife"), atua na área de [educação](http://lattes.cnpq.br/8755541190873634 "Currículo Lattes"), deixou de usar [(...)](# "e ser usado pelas") “redes sociais”, mas me interessa por tecnologia, linguagens e suas diversas modalidades.

Gosta do FreeBSD, mas preza pela liberdade de software, sendo um feliz utilizador do Debian GNU/Linux. Alterna entre o i3-wm e o xmonad; além de se perder no terminal.

É baixo natural [(...)](# "mas queria mesmo era ser barítono"), faz parte do coro Ars Canticus e neste canal no [Odysse](https://odysee.com/@jacksondejesus:1 "Canal pessoal no Odysse") esconde [(...)](# "o nervosismo de") algumas apresentações. É dublê de escritor no blog [Meiotexto](https://meiotexto.wordpress.com/ "Blog Meiotexto no Wordpress") e no [Recanto das letras](https://www.recantodasletras.com.br/autores/literolinguista "Página pessoal no Recanto das letras") e por fim, responde emails em <literolinguista@gmail.com>.

[Voltar ao início](https://literolinguista.prose.sh)
