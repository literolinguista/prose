
> ... nem sempre o vazio é sinônimo de silêncio [(...)](# "às vezes, é pleno de significados").

Oi, escrevo este site [(...)](# "em HTML e CSS puros") para tentar organizar [(...)](# "e publicar") conteúdos relacionados a estudos pessoais, sob a licença Creative Commons. As leituras que me ajudaram estão na página de [referências](referencias "Referências"). A fim de resumir texto, omito informações [(...)](# "irrelevantes") entre parênteses; caso queira ler, basta passar o mouse sobre eles.

Apesar de estático, este site está em constante revisão [(...)](# "e não coleta nenhuma informação")...

**Boa leitura.**

[Voltar ao início](https://literolinguista.prose.sh)
