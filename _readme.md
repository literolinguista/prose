---
title: Literolinguista 
description: Leio e escrevo {códigos}, poesia, (receitas) e prosa.
nav:
    - início: https://literolinguista.prose.sh/inicio
    - projetos: https://literolinguista.prose.sh/projetos
    - referências: https://literolinguista.prose.sh/referencias
    - textos: https://literolinguista.prose.sh/textos
    - sobre: https://literolinguista.prose.sh/sobre
---
**Literolinguista:** s.n. i[e]migrante das fronteiras ilu[irri]sórias da linguística e litera[ora]tura.

Atualizado numa noite chuvosa de quarta-feira (22.02.2023).

Conteúdos sob a [Licença Creative Commons CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)

