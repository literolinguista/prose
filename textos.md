> “Escrever é fácil. Você começa com uma letra maiúscula e termina com um ponto final. No meio você coloca idéias.”
>
> Pablo Neruda.

## Textos inacabados sobre assuntos diversos, sem ordem cronológica ou bases confiáveis ou mesmo nexo...

[Sobre séries marcantes... (22.02.2023 - indicação)](https://literolinguista.prose.sh/sobre-series-marcantes)

[Sobre o tempo... (20.02.2023 - reflexão)](https://literolinguista.prose.sh/sobre-o-tempo)

[Sobre o mito da doutrinação escolar... (13.09.2022 - educação)](https://literolinguista.prose.sh/sobre-doutrinacao)

[Sobre senhas... (28.07.2022 - tecnologia)](https://literolinguista.prose.sh/sobre-senhas)

[Voltar ao início](https://literolinguista.prose.sh)
