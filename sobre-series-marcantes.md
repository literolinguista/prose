
> “Gostar é provavelmente a melhor maneira de ter, ter deve ser a pior maneira de gostar.”
>
> José Saramago.

## Séries que me impressionaram!

Quando sobra um tempo, assisto umas séries [(...)](# "pelo aplicativo Stremio porque sou pobre de dinheiro, rs"). Estas são algumas, [(...)](# "mais ou menos recentes") que me marcaram.

- [Arcane](https://www.adorocinema.com/series/serie-29127 "Arcane")
- [Fundação](https://www.adorocinema.com/series/serie-18438 "Fundação")
- [Halo](https://www.adorocinema.com/series/serie-12209 "Halo")
- [Love, death & robots](https://www.adorocinema.com/series/serie-24635 "Love, death & robots")
- [O senhor dos Anéis: Os aneis do poder](https://www.adorocinema.com/series/serie-22940 "O senhor dos Anéis: Os aneis do poder")
- [Periféricos](https://www.adorocinema.com/series/serie-23551 "Periféricos")
- [Raised by wolves](https://www.adorocinema.com/series/serie-24255 "Raised by wolves")
- [The boys](https://www.adorocinema.com/series/serie-22668 "The boys")
- [The nevers](https://www.adorocinema.com/series/serie-23845 "The nevers")
- [The witcher](https://www.adorocinema.com/series/serie-22146 "The witcher")
- [Wandinha](https://www.adorocinema.com/series/serie-28487 "Wandinha")

A maioria delas, indicações do canal [Futurices](https://www.youtube.com/@Futurices), da Bela Eichler (pense numas resenhas excelentes!).

[Voltar para a página Textos](https://literolinguista.prose.sh/textos)
